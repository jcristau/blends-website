#!/bin/sh
./aging_packages_of_blend -m debian-med
# remove info about packages we have just checked and realised that we
# can not do much about it to update it.  Responsible maintainers are
# informed (in most cases)
grep -v \
        -e "^ cfflib "             \
        -e "^ eegdev "             \
        -e "^ imview "             \
        -e "^ imagetooth "         \
        -e "^ medicalterms "       \
        -e "^ psignifit "          \
        -e "^ pvclust "            \
        -e "^ pyepl "              \
        -e "^ python-casmoothing " \
        -e "^ python-clips "       \
        -e "^ pyxid "              \
        -e "^ trimage "            \
        debian-med.out
