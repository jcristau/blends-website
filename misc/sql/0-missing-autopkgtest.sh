#!/bin/sh
# seek for packages without autopkgtest ordered by popcon
#
# This query gives a list of source packages ordered by votes of binary
# package with maximum vote and the availability of a test suite

usage() {
cat >/dev/stderr <<EOT
Usage: $0 [option] <blend> <task>
      -h this help screen
      -m forcing public mirror over local one

Description:
  Query UDD for packages belonging to a task of a specified blend.
  The output is sorted according to usage statistics and contains
  information about existence of an autopkgtest as well whether the
  interface might require X11 or not.
EOT
}

if [ ! $(which psql) ] ; then
    cat <<EOT
No PostgreSQL client providing /usr/bin/psql installed.
On a Debian system please
    sudo apt-get install postgresql-client-common
EOT
    exit 1
fi

SERVICE="service=udd"
#if there is a local UDD clone just use this
if psql $PORT -l 2>/dev/null | grep -qw udd ; then
    SERVICE=udd
fi

# Check UDD connection
if ! psql $PORT $SERVICE -c "" 2>/dev/null ; then
    echo "No local UDD found, use public mirror."
    export PGPASSWORD="public-udd-mirror"
    SERVICE="--host=public-udd-mirror.xvm.mit.edu --port=5432 --username=public-udd-mirror udd"
fi

while getopts "hjm" o; do
    case "${o}" in
        h)
            usage
            exit 0
            ;;
        m)
           export PGPASSWORD="public-udd-mirror"
           SERVICE="--host=public-udd-mirror.xvm.mit.edu --port=5432 --username=public-udd-mirror udd"
           ;;
        *)
            usage
            exit 1
            ;;
    esac
done
shift $((OPTIND-1))

if [ $# -ne 2 ] ; then
    usage
    exit 1
fi

psql $SERVICE <<EOT
SELECT source, vote, MAX(testsuite) as testsuite, tags FROM (
  SELECT source, vote, testsuite, tags, row_number() OVER (PARTITION BY source ORDER BY vote DESC) FROM (
    SELECT DISTINCT p.source, p.package, CASE WHEN pop.vote IS NULL THEN -1 ELSE pop.vote END AS vote, s.testsuite, d.tags FROM packages p
      LEFT OUTER JOIN popcon  pop ON p.package = pop.package
      LEFT OUTER JOIN (SELECT source, testsuite FROM sources WHERE release = 'sid' ) s   ON p.source  = s.source
      LEFT OUTER JOIN (SELECT package, array_agg(tag) AS tags FROM debtags WHERE tag LIKE 'uitoolkit::%' OR tag LIKE '%x11%' GROUP BY package) d ON p.package = d.package
      WHERE p.package IN (
        SELECT package FROM blends_dependencies WHERE blend = '$1' AND task = '$2'
      )
      AND p.release = 'sid'
  ) tmp1
) tmp2
GROUP BY source, vote, tags
ORDER BY vote DESC, source
  ;
EOT

exit 0
