#!/usr/bin/python3

import gitlab
import os
import sys
import shutil
import base64
import subprocess
import time

BLENDSGROUPS={ 'Debian 3D Printing Team'       : '3dprinter'
             , 'Debian Accessibility Team'     : 'pkg-a11y'
             , 'Debian Astro Team'             : 'debian-astro'
             , 'Debian Edu Packaging Team'     : 'debian-edu'
             , 'Debian Electronics'            : 'pkg-electronics'
             , 'Debian Games'                  : 'pkg-games'
             , 'Debian GIS Project'            : 'pkg-grass'
             , 'Debian Hamradio Maintainers'   : 'pkg-hamradio'
             , 'Debian IoT'                    : 'pkg-iot'
             , 'Debian Java Maintainers'       : 'pkg-java'
             , 'Debian JavaScript Maintainers' : 'pkg-js'
             , 'Debian Med'                    : 'debian-med'
             , 'Debian Multimedia Team'        : 'pkg-multimedia'
             , 'Debian Octave Group'           : 'pkg-octave'
             , 'Debian Perl Group'             : 'pkg-perl'
             , 'Debian PhotoTools Team'        : 'pkg-phototools'
             , 'Debian R Packages Maintainers' : 'pkg-r'
             , 'Debian Science Team'           : 'debian-science'
             , 'Debian Tryton Maintainers'     : 'tryton'
             , 'Debian VoIP Packaging Team'    : 'pkg-voip'
             , 'Debichem'                      : 'debichem'
             }

# Do not parse these projects which are no real packages
BLACKLIST = [  'bio-linux'
             , 'communication'
             , 'helper-scripts'
             , 'package_template'
             , 'papers'
             , 'policy'
             , 'scripts'
             , 'website'
            ]

# missing on Salsa yet:
#            pkg-exppsy
#            Python
# FIXME: Debian Perl Group is listed with only 2 projects which is definitely wrong

# just start with one team
# BLENDSGROUPS={ 'Debian Med'                    : 'debian-med'}

debianmetadata = [ 'changelog',
                   'control',
                   'copyright',
                   'README.Debian'
                 ]
upstreammetadata = [ 'edam',
                     'metadata'
                   ]

TDNAME='machine-readable'
MACHINEREADABLEARCHIVE='/srv/blends.debian.org/www/_'+TDNAME+'/'+TDNAME+'.tar.xz'
READMEDEBIANARCHIVE='/srv/blends.debian.org/www/_'+TDNAME+'/README.Debian.tar.xz'
TARGETDIR=os.path.join(os.environ['HOME'],TDNAME)

SLEEPTIMEFILES=5
SLEEPTIMEPROJECTS=5
SLEEPUNTILRETRY=3600
debug=1

# do not clean dirs.  FIXME: Find a method to detect removed repositories and clean this up here
try:
    os.makedirs(TARGETDIR)
except:
    pass

def wait_for_salsa():
    print("Received GitlabGetError: 429 - Retry later ... waiting for %i seconds" % SLEEPUNTILRETRY )
    time.sleep(SLEEPUNTILRETRY)

def get_blends_groups():
    blends_groups=[]
    groups = gl.groups.list(all=True, order_by='name', sort='asc')
    for group in groups:
        # print(group.attributes['name'])
        if group.attributes['name'] in BLENDSGROUPS:
            blends_groups.append(group)
    return blends_groups

def output_metadata(subdir, metadata):
    try:
        items = project.repository_tree(path=subdir)
    except gitlab.exceptions.GitlabGetError as err:
        print("%s/%s does not seem to have dir %s" % (gpath, name, subdir), file=sys.stderr)
        return None
    except:
        print("%s/%s does not seem to have dir %s" % (gpath, name, subdir), file=sys.stderr)
        print("Unexpected error:", sys.exc_info())
        return None
    for item in items:
        if item['name'] in metadata:
            try:
                file_info = project.repository_blob(item['id'])
            except gitlab.exceptions.GitlabGetError:
                wait_for_salsa()
                file_info = project.repository_blob(item['id'])
            content = base64.b64decode(file_info['content'])
            if item['name'] == 'metadata':
                ext = '.upstream'
            else:
                ext = '.'+item['name']
            with open(os.path.join(namedir,name+ext), 'wb') as out:
                out.write(content)
            out.close()
    return True

# SALSA_TOKEN=os.environ['SALSA_TOKEN']

gl = gitlab.Gitlab("https://salsa.debian.org") # , private_token=SALSA_TOKEN) # anonymous access is fine

LAST_ACTIVITY='last_activity_at: '
LAST_ACTIVITY_LENGTH=len(LAST_ACTIVITY)

blends_groups = get_blends_groups()
for group in blends_groups:
    unchanged, changed, new, ignored = (0, 0, 0, 0)
    gpath = group.attributes['path']
    blend = BLENDSGROUPS[group.attributes['name']]
    projects = group.projects.list(all=True, order_by='name', sort='asc')
    # DEBUG : only few projects to be faster
    # projects = group.projects.list(page=1, per_page=10, order_by='name', sort='asc')
    print("%s has %i projects (Blend name %s)" % (group.attributes['name'], len(projects), blend)) #  group.attributes['id'], group.attributes['path']) # , group.attributes['description'], group.attributes['full_name'])
    for pr in projects:
        try:
            project = gl.projects.get(pr.attributes['id']) # without this extra get repository_tree() fails
        except gitlab.exceptions.GitlabGetError:
            wait_for_salsa()
            project = gl.projects.get(pr.attributes['id']) # without this extra get repository_tree() fails
        name = project.attributes['name']
        time.sleep(SLEEPTIMEPROJECTS)
        if name in BLACKLIST or name.endswith('.pages.debian.net'):
            ignored += 1
            if debug > 0:
                print("Ignore project %s of blend %s." % (name, blend))
            continue
        namedir = os.path.join(TARGETDIR, name[0])
        if not os.path.exists(namedir):
            os.makedirs(namedir)
        dosomething=True
        try:
            # try reading file - if not exists its just written
            invcs = open(os.path.join(namedir,name+'.vcs'), 'r')
            for line in invcs.readlines():
                if line.startswith(LAST_ACTIVITY):
                    last_activity_at = line[LAST_ACTIVITY_LENGTH:].strip()
                    if last_activity_at == project.attributes['last_activity_at']:
                        dosomething=False
                        unchanged += 1
                        if debug > 1:
                            print("Do nothing in %s since found last_activity_at %s == repository %s" % (name, last_activity_at, project.attributes['last_activity_at']))
                    else:
                        if debug > 0:
                            print("Continue with %s since found last_activity_at %s != repository %s" % (name, last_activity_at, project.attributes['last_activity_at']))
                        changed += 1
                invcs.close()
        except FileNotFoundError:
            new += 1
            pass
        if not dosomething:
            continue
        if debug > 1:
            print("Writing %s." % os.path.join(namedir,name+'.vcs'))
        with open(os.path.join(namedir,name+'.vcs'), 'w') as out:
            out.write("Vcs-Browser: https://salsa.debian.org/%s/%s\n" % (gpath, name))
            out.write("Vcs-Git: https://salsa.debian.org/%s/%s.git\n" % (gpath, name))
            out.write("Blend: %s\n" % blend)
            out.write("%s%s\n" % (LAST_ACTIVITY, project.attributes['last_activity_at']))
        out.close()
        # print(project)
	# be friendly to Salsa and add some delay timr before real reading of data
        # see thread around https://lists.debian.org/debian-devel/2018/07/msg00125.html
        time.sleep(SLEEPTIMEFILES)
        if output_metadata('debian', debianmetadata):
            output_metadata('debian/upstream', upstreammetadata)
    print("In %s %i repositories changed, %i remained unchanged, %i were new and %i were ignored." % (blend, changed, unchanged, new, ignored))

# os.system("tar --exclude=*README.Debian -caf %s %s" % (MACHINEREADABLEARCHIVE, TARGETDIR))
p = subprocess.Popen(['tar', '--exclude=*README.Debian', '-caf', MACHINEREADABLEARCHIVE, TDNAME], cwd=os.environ['HOME'])
p.wait()
# os.system("tar --exclude=*.control --exclude=*.changelog --exclude=*.upstream --exclude=*.edam --exclude=*.vcs --exclude=*.copyright -caf %s %s" % (READMEDEBIANARCHIVE, TARGETDIR))
p = subprocess.Popen(['tar', '--exclude=*.control', '--exclude=*.changelog', '--exclude=*.upstream', '--exclude=*.edam', '--exclude=*.vcs', '--exclude=*.copyright', '-caf', READMEDEBIANARCHIVE, TDNAME], cwd=os.environ['HOME'])
p.wait()
