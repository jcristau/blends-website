#!/bin/sh -e

: <<=cut

=head1 NAME

check-all-tasks - check whether there are changes of tasks files in Vcs

=head1 SYNOPSIS

check-all-tasks

=head1 DESCRIPTION

This script checks whether there is any need to update web sentinel
pages.  It checkout / clones the relevant repositories that are
mentioned in the according config file in `pwd`/webconf.

=cut

if [ "--help" = "$1" -o "-h" = "$1" ] ; then
	pod2man --center "Debian Blends Documentation" check-all-tasks  | nroff -man
	exit 1
fi

cd `dirname $0`

needsupdate=0

for bc in `ls webconf/*.conf | grep -v -e "webconf/debug\." -e rest-test` ; do
    blend=`basename ${bc} .conf`
    # echo "----- $blend ------"
    datadir=`grep "^DataDir:" ${bc} | sed 's/^DataDir:\s*//'`
    datasubdir=`echo $datadir | sed 's?^.*/\([^/]\+\)$?\1?'`
    vcsurl=`grep "^VcsDir:" ${bc} | sed 's/^VcsDir:\s*//'`
    vcstype=`echo ${vcsurl} | sed 's/^\([a-z]\{3\}\):.*/\1/'`
    if [ "$vcstype" != "git" -a "$vcstype" != "svn" ] ; then
        if echo ${vcsurl} | grep -q -e '^https://github' -e '^https://salsa.debian.org' ; then
            vcstype=git
        else
            vcstype=unknown
            echo "Unknown VcsType ${vcstype} in VcsURL ${vcsurl}"
        fi
    fi
    [ ! -d `dirname ${datadir}` ] && mkdir -p `dirname ${datadir}`
    curdir=`pwd`
    md5file="../${blend}.md5"
    if [ ! -d ${datadir} ] ; then
        if [ "${vcstype}" = "git" ] ; then
            cd `dirname ${datadir}`
            git clone --quiet ${vcsurl} ${datasubdir} 2>/dev/null && true
            if [ $? -gt 0 ] ; then
                # echo "Unable to fetch initial data for $blend from $VcsDir - try without SSL verification" ;
                GIT_SSL_NO_VERIFY=1 git clone  --quiet ${vcsurl}  ${datasubdir} >/dev/null && true
            fi
        else if [ "${vcstype}" = "svn" ] ; then
                mkdir -p ${datadir}
                cd ${datadir}
                svn --quiet checkout ${vcsurl}/debian
                svn --quiet checkout ${vcsurl}/tasks
            fi
        fi
    fi
    if [ ${vcsurl} != "unknown" ] ; then
        cd ${datadir}
        # echo "vcs clone of ${vcsurl} exists in ${datadir}"
        if [ "${datadir}" = "" ] ; then
            echo "datadir is empty"
            echo ${vcsurl}
    	    exit 1
        fi
        if [ "${vcstype}" = "git" ] ; then
            git pull --quiet 2>/dev/null && true
            if [ $? -gt 0 ] ; then
                # echo "Unable to pull data for $blend from $VcsDir - try without SSL verification"
                GIT_SSL_NO_VERIFY=1 git pull >/dev/null 2>/dev/null && true
            fi
        else if [ "${vcstype}" = "svn" ] ; then
                cd tasks
                svn --quiet up
                cd ..
            else
                echo "Unknown VcsType ${vcstype} in VcsURL ${vcsurl}"
            fi
        fi
        if [ ! -e ${md5file} -a -d tasks ] ; then
    	    cat `find tasks -mindepth 1 -maxdepth 1 -type f | sort` | md5sum > ${md5file}
        else
            mv ${md5file} ${md5file}.old
            if [ -d tasks ] ; then
  	        cat `find tasks -mindepth 1 -maxdepth 1 -type f | sort` | md5sum > ${md5file}
  	    fi
    	    if ! diff ${md5file} ${md5file}.old >/dev/null ; then
    		# echo "Difference in tasks files of Blend ${blend}"
    		cd $curdir
                if ! ps aux | grep -v "0:0[0-9] grep " | grep -v "0:0[0-9] ps " | grep tasks.py ; then
                    ./tasks.py ${blend}
                    needsupdate=1
                else
                    echo "Script tasks.py seems to be running currently.  No update."
                fi
    		if ! ps aux | grep -v "0:0[0-9] grep " | grep -v "0:0[0-9] ps " | grep thermometer.py ; then
                    ./thermometer.py ${blend}
                    needsupdate=1
                else
                    echo "Script thermometer.py seems to be running currently.  No update."
                fi
    	    fi
        fi
    fi
    cd $curdir
done

if [ $needsupdate -eq 1 ] ; then
    [ -x /usr/local/bin/static-update-component ] && /usr/local/bin/static-update-component blends.debian.org >/dev/null 2>/dev/null
fi
